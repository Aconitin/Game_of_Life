        -- CCCCCCCCCCCCC                                      ffffffffffffffff  
     -- CCC::::::::::::C                                     f::::::::::::::::f 
   -- CC:::::::::::::::C                                    f::::::::::::::::::f
  -- C:::::CCCCCCCC::::C                                    f::::::fffffff:::::f
 -- C:::::C       CCCCCC   ooooooooooo   nnnn  nnnnnnnn     f:::::f       ffffff
-- C:::::C               oo:::::::::::oo n:::nn::::::::nn   f:::::f             
-- C:::::C              o:::::::::::::::on::::::::::::::nn f:::::::ffffff       
-- C:::::C              o:::::ooooo:::::onn:::::::::::::::nf::::::::::::f       
-- C:::::C              o::::o     o::::o  n:::::nnnn:::::nf::::::::::::f       
-- C:::::C              o::::o     o::::o  n::::n    n::::nf:::::::ffffff       
-- C:::::C              o::::o     o::::o  n::::n    n::::n f:::::f             
 -- C:::::C       CCCCCCo::::o     o::::o  n::::n    n::::n f:::::f             
  -- C:::::CCCCCCCC::::Co:::::ooooo:::::o  n::::n    n::::nf:::::::f            
   -- CC:::::::::::::::Co:::::::::::::::o  n::::n    n::::nf:::::::f            
     -- CCC::::::::::::C oo:::::::::::oo   n::::n    n::::nf:::::::f            
        -- CCCCCCCCCCCCC   ooooooooooo     nnnnnn    nnnnnnfffffffff            

function love.conf(t)
    t.title = "Linked"
    t.identity = "Linked"
    t.version = "0.9.0"
    t.console = true
    t.window.width = 1200
    t.window.height = 800
    t.window.fullscreen = false
    t.window.vsync = false
    t.window.fsaa = 0
	t.window.borderless = false
	t.window.resizable = false
	t.window.centered = true
    t.modules.joystick = true
    t.modules.audio = true
    t.modules.keyboard = true
    t.modules.event = true
    t.modules.image = true
    t.modules.graphics = true
    t.modules.timer = true
    t.modules.mouse = true
    t.modules.sound = true
    t.modules.physics = true
end