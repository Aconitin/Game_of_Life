vars = {}
vars.loopLimit = 1
vars.maxLoopLimit = vars.loopLimit
vars.canv = {}
vars.Scale = 1

function love.mousepressed(x, y, btn)
	if btn == 'wu' and vars.loopLimit < 21 then
		vars.loopLimit = vars.loopLimit + 1
		if vars.loopLimit > vars.maxLoopLimit then
			vars.maxLoopLimit = vars.loopLimit
		end
		if vars.loopLimit > 21 then
			vars.loopLimit = 21
		end
		vars.rd = coroutine.create(redraw)
	elseif btn == 'wd' then
		if vars.loopLimit ~= 1 then
			vars.loopLimit = vars.loopLimit - 1
			vars.rd = coroutine.create(redraw)
		end
	end
end

function redraw()
	print("----------------------")
	print("Switching iteration...")
	if not vars.canv[vars.loopLimit] then
		print("     Generating canvas...")
		local v = {}
		v.hx, v.lx, v.ptbl = render(dragon(), 4, 4, 1)
		print("     Curve size in pixels: " .. v.hx .. "  ||  " .. v.lx)
		local vsx, vsy = 1, 1
		if v.hx > love.graphics.getWidth() then
			vsx = love.graphics.getWidth()/v.hx
		end
		if v.lx > love.graphics.getHeight() then
			vsy = love.graphics.getHeight()/v.lx
		end
		if vsx < vsy then
			vars.Scale = vsx
		elseif vsx >= vsy then
			vars.Scale = vsy
		end
		v.tbl = {}
		print("     Pushing new lines...")
		local ID = 1
		v.tbl[ID] = {}
		for i,k in ipairs(v.ptbl) do
			if i%100000 == 0 then
				ID = ID + 1
				v.tbl[ID] = {}
			end
			table.insert(v.tbl[ID], k[1]*vars.Scale)
			table.insert(v.tbl[ID], k[2]*vars.Scale)
		end
		v.ptbl = nil
		collectgarbage()
		print("     Rendering to canvas...")
		vars.canv[vars.loopLimit] = love.graphics.newCanvas()
		while ID >= 1 do
			vars.canv[vars.loopLimit]:renderTo(function()
				love.graphics.line(v.tbl[ID])
			end)
			v.tbl[ID] = nil
			collectgarbage()
			ID = ID - 1
			coroutine.yield()
		end
		v.tbl = nil
		collectgarbage()
	end
	print("     Collecting garbage...")
	for i = 1, vars.maxLoopLimit, 1 do
		if i ~= vars.loopLimit then
			vars.canv[i] = nil
		end
	end
	vars.maxLoopLimit = vars.loopLimit
	collectgarbage()
	print("Done.")
	print("----------------------")
end

function love.draw()
	if vars.canv[vars.loopLimit] then
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.draw(vars.canv[vars.loopLimit], 0, 0, 0, 1, 1)
	end
end

function love.update()
	if coroutine.status(vars.rd) == "suspended" then
		coroutine.resume(vars.rd)
	end
	love.window.setTitle("Dragon Curve by Aconitin  ||  FPS: " .. love.timer.getFPS() .. "  ||  Iteration: " .. vars.loopLimit .. "  ||  MaxIteration: 21  ||  Use Mousewheel to generate the curves.")
end

function dragon()
    local l = "l"
    local r = "r"
    local inverse = {l = r, r = l}
    local field = {r}
    local num = 1
    for discard=1,vars.loopLimit do
        field[num+1] = r
        for i=1,num do
            field[i+num+1] = inverse[field[num-i+1]]
			if num%10000 == 0 then
				coroutine.yield()
			end
        end
        num = num*2+1
		coroutine.yield()
    end
    return field
end

function render(field, w, h, l)
    local x = 0
    local y = 0
    local points = {}
    local highest_x = 0
    local highest_y = 0
    local lowest_x = 0
    local lowest_y = 0
    local l = "l"
    local r = "r"
    local u = "u"
    local d = "d"
    local heading = u
    local turn = {r = {r = d, d = l, l = u, u = r}, l = {r = u, u = l, l = d, d = r}}
    for k, v in ipairs(field) do
        heading = turn[v][heading]
        for i=1,3 do
            points[#points+1] = {x, y}
            if heading == l then
                x = x-w
            elseif heading == r then
                x = x+w
            elseif heading == u then
                y = y-h
            elseif heading == d then
                y = y+h
            end
            if x > highest_x then
                highest_x = x
            elseif x < lowest_x then
                lowest_x = x
            end
            if y > highest_y then
                highest_y = y
            elseif y < lowest_y then
                lowest_y = y
            end
        end
    end
    points[#points+1] = {x, y}
    highest_x = highest_x - lowest_x + 1
    highest_y = highest_y - lowest_y + 1
    for k, v in ipairs(points) do
        v[1] = v[1] - lowest_x + 1
        v[2] = v[2] - lowest_y + 1
    end
    return highest_x, highest_y, points
end

vars.rd = coroutine.create(redraw)